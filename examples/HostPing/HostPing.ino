/*
 * This example show how to ping a remote machine using it's hostname
 */

#include <ESP8266WiFi.h>
#include <ESP8266Ping.h>

#define LED 2

const char* ssid = "Nobody";
const char* password = "michelangelo";

const char* remote_host = "www.google.com";
unsigned long timestamp_poll;
unsigned long timestamp_blink;
unsigned long timestamp_blink2;
char state = 0;
char ping_ok = 0;

void setup()
{
    Serial.begin(115200);
    delay(10);

    // We start by connecting to a WiFi network

    pinMode(LED, OUTPUT);
    Serial.println();
    Serial.println("Connecting to WiFi");

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(100);
        Serial.print(".");
    }

    Serial.println();
    Serial.print("WiFi connected with ip ");
    Serial.println(WiFi.localIP());

    Serial.print("Pinging host ");
    Serial.println(remote_host);

    timestamp_poll = millis() + 60*1000;
    timestamp_blink = millis() + 100;
    timestamp_blink2 = millis() + 100;
    ping_ok = 1;
    digitalWrite(LED, LOW); 
    delay(100);
    digitalWrite(LED, HIGH); 
}

void loop() 
{
    if (millis() > timestamp_poll) {
        timestamp_poll = millis() + 60*1000;
        if (Ping.ping(remote_host)) {
            Serial.println("Success!!");
            ping_ok = 1;
        }
        else {
            Serial.println("Error :(");
            ping_ok = 0;
        }
    }

    if (millis() > timestamp_blink) {
        timestamp_blink = millis() + 100;
        
        if (0 == ping_ok) {
            state = state > 0 ? 0 : 1;
            digitalWrite(LED, (state > 0) ? HIGH: LOW); 
        }
    }
    if (millis() > timestamp_blink2) {
        timestamp_blink2 = millis() + 5*1000;

        if (1 == ping_ok) {
            digitalWrite(LED, LOW); 
            delay(100);
            digitalWrite(LED, HIGH); 
        }
    }
}








